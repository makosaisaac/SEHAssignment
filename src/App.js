import React, { useEffect, useState, useRef } from 'react';
import './App.scss';
import { Canvas,useFrame,useThree} from "react-three-fiber";
import lerp from 'lerpjs';
import nust from "./images/nust2.png";

import goal1_sta from "./images/goal1_sta.jpg";
import goal1_key from "./images/goal1_key.jpg";
import goal2_sta from "./images/goal2_sta.jpg";
import goal2_key from "./images/goal2_key.jpg";
import goal3_sta from "./images/goal3_sta.jpg";
import goal3_key from "./images/goal3_key.jpg";


import nust_log1 from "./images/nust_logo1.png";
import nust_log2 from "./images/nust_logo2.png";
import defaultpic from "./images/defalut.jpg";



// import { Physics, usePlane, useBox } from '@react-three/cannon';

// import * as CANNON from 'cannon-es';
import * as THREE from 'three';
import { OrbitControls, Sky, Stars, MapControls } from "@react-three/drei";
// import Boy from "./Mychar";

// import { Provider, useCannon} from './useCannon'
// import { RoundedBox } from "drei";

// const Geo = ({ position }) => {
//   const mesh = useRef(null);
//   //  useFrame(() => (mesh.current.rotation.x = mesh.current.rotation.y += 0.01));

//   // const ref = useCannon({ mass: 100000 }, body => {
//   //   body.addShape(new CANNON.Box(new CANNON.Vec3(1, 1, 1)))
//   //   body.position.set(...position)
//   // })

//   // const [ref] = useBox(()=>({mass:1,position:[0,5,0]}));
//   return (
//     <mesh refer={mesh} position={position} castShadow receiveShadow>
//       <boxBufferGeometry attach='geometry' args={[1, 1, 1]} />
//       {/* <circleBufferGeometry attach='geometry' args={[1, 50]} /> */}
//       <meshStandardMaterial attach='material' color='pink' />
//     </mesh>
//   );
// }


// const Plane = ({ position }) => {
//   // Register plane as a physics body with zero mass
//   // const ref = useCannon({ mass: 0 }, body => {
//   //   body.addShape(new CANNON.Plane())
//   //   body.position.set(...position,[-Math.PI/2,0,0])
//   // })
//   // const [ref] =  usePlane(()=>({
//   //   position:[0,5,0],
//   //   rotation:[-Math.PI/2,0,0]
//   // }))
//   const mesh = useRef(null);
//   useFrame(() => (mesh.current.rotation.z += 0.01))
//   // const [ref] = usePlane(() => ({ rotation: [-Math.PI / 2, 0, 0]}))
//   return (
//     <mesh ref={mesh} position={position} receiveShadow rotation={[-Math.PI / 2, 0, 0]}>
//       <planeBufferGeometry attach="geometry" args={[100, 100]} />
//       <meshStandardMaterial attach="material" color="lightblue" />
//     </mesh>
//   )
// }


const Starss = ({mouse,hover})=>{
  const ref = useRef()
  const { size, viewport } = useThree()
  const aspect = size.width / viewport.width
  useFrame(state => {
    if (ref.current) {
      ref.current.position.x = lerp(ref.current.position.x, mouse.current[0] / aspect / 10, 0.1)
      ref.current.rotation.x = lerp(ref.current.rotation.x, 0 + mouse.current[1] / aspect / 50, 0.1)
      ref.current.rotation.y = 0.8
    }
  })
  return(
    <Stars ref={ref} fade></Stars>
  )

}


function App() {
  // const [set] = useState(true);

  // useEffect(() => void setTimeout(() => set(false), 5000));
  useEffect(() => {
    document.title = "NUST GOALS"
  }, []);

  document.addEventListener("DOMContentLoaded", function () {
    
    var startBut = document.querySelector(".startGame");
    var main = document.querySelector(".main");
    var nust_logo = document.querySelector(".nust_logo");
    var logo = document.querySelector(".logo");
    
    let game = document.querySelector(".gameWindow")
    let piece = game.querySelectorAll("div");
    let pos = 0;
    let emptypiece;
    let url = defaultpic;
    const hint = document.querySelector(".hintWindow");
    const stra = document.querySelector(".strategicSelect");

    startBut.addEventListener('click',()=>{
      startBut.style.display = "none";
      nust_logo.style.display = "none";
      main.style.display = "flex";
      logo.style.display = "flex";
    })

    let classList = ["empty", "puzzle2", "puzzle3", "puzzle4", "puzzle5", "puzzle6", "puzzle7", "puzzle8", "puzzle9"];


    function removeClasses() {
      for (let i = 0; i < piece.length; i++) {
        piece[i].classList.remove(piece[i].classList[0]);
        piece[i].style.backgroundImage = "";
        piece[i].style.backgroundSize = "";
        piece[i].style.backgroundColor = "";
      }
    }

    function randomClasses() {
      let numberOcuppied = [];

      for (let i = 0; i < piece.length; i++) {
        piece[i].style.backgroundImage = "";
        piece[i].style.backgroundSize = "";
        piece[i].style.backgroundColor = "";
        if (piece[i].classList.length === 1) {

          piece[i].classList.remove(piece[i].classList[0]);
        }
      }

      while (numberOcuppied.length < 9) {
        let classNum = Math.floor(Math.random() * 9);
        if (numberOcuppied.indexOf(classNum) === -1) {
          piece[numberOcuppied.length].classList.add(classList[classNum]);
          numberOcuppied.push(classNum);
          //				
          //				getPicture();
        }
      }
    }

    function start() {
      for (let i = 0; i < piece.length; i++) {
        console.log('Start', piece.length)
        // eslint-disable-next-line no-loop-func
        piece[i].addEventListener("click", function (event) {
          let n = [...piece].indexOf(event.target);
          let empty = document.querySelector(".empty");
          let emptyIndex = [...piece].indexOf(empty);
          if (emptyIndex + 1 === n || emptyIndex + 3 === n || emptyIndex - 1 === n || emptyIndex - 3 === n) {
            empty.classList.remove(empty.classList[0]);
            empty.classList.add(event.target.classList[0]);
            event.target.classList.add("empty");
            event.target.classList.remove(event.target.classList[0]);

            for (let i = 0; i < piece.length; i++) {
              piece[i].style.backgroundImage = `url(` + url + `)`;
            }

            emptypiece = game.querySelector(".empty");
            emptypiece.style.backgroundImage = "none";
            emptypiece.style.backgroundColor = "white";

          } else {
            console.log("nie ma go tutaj");
          }
        })
      }
    }

    function getPicture(selectedVal) {
      // eslint-disable-next-line default-case
      switch (selectedVal) {
        case "1":
          console.log(selectedVal)
          if(stra.value === "sta")
              url = goal1_sta;
          else
              url = goal1_key;
          break;
        case "2":
          console.log(selectedVal)      
          if(stra.value === "sta")
          url = goal2_sta;
          else
          url = goal2_key;
          break;
        case "3":
          if(stra.value === "sta")
          url = goal3_sta;
          else
          url = goal3_key;
          break;
        case "4":
          console.log(selectedVal)
          break;
        case "5":
          console.log(selectedVal)
          break;
      }

      for (let i = 0; i < piece.length; i++) {
        piece[i].style.backgroundImage = `url(` + url + `)`;
        piece[i].style.backgroundSize = '600px';
      }
      emptypiece = document.querySelector(".empty");
      emptypiece.style.backgroundImage = "none";

      hint.style.backgroundImage = `url(` + url + `)`;
      hint.style.backgroundSize = 'cover';
    }

    function defaultClassesOrder() {
      for (let i = 0; i < piece.length; i++) {
        piece[i].classList.add(classList[i]);
      }
    }


    const select = document.querySelector(".picSelect");
    let selectedVal = select.value;

    select.addEventListener("change", function () {
      selectedVal = select.value;
      removeClasses();
      defaultClassesOrder();
      getPicture(selectedVal);
    })

    stra.addEventListener("change", function () {
      selectedVal = select.value;
      removeClasses();
      defaultClassesOrder();
      getPicture(selectedVal);
    })

    const startBtn = document.querySelector(".startBtn");
    startBtn.addEventListener("click", function () {
      removeClasses();
      randomClasses();
      getPicture(selectedVal);
      start();
    })

    defaultClassesOrder();
    getPicture(selectedVal);
  });

  const mouse = useRef([0, 0]);
  const [hover] = useState(false)

  return (
    <>

    <div className="nust_logo">
        <img src={nust_log1}/>

        <img src={nust_log2}/>

        <div className="startGame"> <span>Start</span> <span>Game</span></div>
    </div>

      <div className="main">

        <div className="gameDiv">

          <div className="gameWindow">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
          </div>

        </div>

        <div className="data">

          <div className="gameName">

                <span>N</span>&nbsp;
                <span>U</span> &nbsp;
                <span>S</span>&nbsp;
                <span>T</span>&nbsp;&nbsp;&nbsp;
                {/* <span>PUZZLE</span> */}
                <span>GOALS</span>&nbsp;&nbsp;&nbsp;
                <span>2</span>&nbsp;
                <span>0</span>&nbsp;
                <span>2</span>&nbsp;
                <span>1</span>&nbsp;
          </div>

        <div className="hintWindow">
            {/* <div className="hint">
                <span>H</span>&nbsp;
                <span>I</span> &nbsp;
                <span>N</span>&nbsp;
                <span>T</span>&nbsp;
            </div> */}
        </div>


          
        <div className="choose-goals">

          <select className="picSelect">
            <option>Choose a Goal</option>
            <option value="1">Goal 1</option>
            <option value="2">Goal 2</option>
            <option value="3">Goal 3</option>
            <option value="4">Goal 4</option>
            <option value="5">Goal 5</option>
          </select>

          <select className="strategicSelect">
            <option value="sta">Strategic Objectives</option>
            <option value="key">Key Perfomance Indicators</option>
          </select>
          </div>
          <button className="startBtn">PLAY</button>

        </div>
   </div>


      <div className="logo">
              <div className="picname">
                <img src={nust}></img>
              </div>
      </div>

      <Canvas colorManagement
        camera={{ position: [0, 10, 30], fov: 60 }}
        shadowMap onCreated={({ gl }) => {
          gl.toneMapping = THREE.ACESFilmicToneMapping
          gl.outputEncoding = THREE.sRGBEncoding
        }}
      >
        <OrbitControls onMouseDown={mouse}/>
        <MapControls />
        {/* <Stars /> */}

        <Starss mouse={mouse} hover={hover}/>
        
         {/* <Sky/> */}
        <ambientLight intensity={0.3} />
        <directionalLight position={[0, -10, 0]} />
        <pointLight position={[100, 100, 30]} intensity={0.2} />
        <spotLight intensity={0.6} position={[10, 200, 20]} angle={0.2} penumbra={1} castShadow />

        {/* <Provider> */}
        {/* <Physics  iterations={15} gravity={[0, -200, 0]} allowSleep={false}> */}
        {/* <Plane position={[0,0,0]} rotation={[-Math.PI/2,0,0]}/> */}
        {/* {showPlane && <Plane position={[0, 0, 0]} />} */}
        {/* <Geo position={[0,10,0]}/> */}
        {/* <Geo position={[3,1,0]}/> */}
        {/* <Geo position={[0,6,0]}/>
              <Geo position={[-4,1,0]}/>
              <Geo position={[-5,6,0]}/> */}
        {/* {!showPlane && <Geo position={[0.5, 1.0, 0]} />} */}

        {/* <Suspense fallback={null}>
                  <Boy/>
              </Suspense> */}
        {/* </Physics> */}

        {/* </Provider> */}

      </Canvas>

    </>
  );
}

export default App;
